# Generate Video from Images and Audio (gvc)

`gvc` is a Python-based command-line tool that creates video slideshows by combining a collection of images and an audio file. The generated video file can be uploaded to popular video-sharing platforms, such as YouTube or Vimeo.

## Features

- Supports common audio formats like MP3, WAV, and FLAC
- Supports common image formats like JPEG, JPG, and PNG
- Automatically calculates image display duration based on the total duration of the audio file
- Generates an output video file in MP4 format, compatible with popular video-sharing platforms
- Utilizes the moviepy library for efficient audio and video processing
- Includes a dissolve effect between images for smooth transitions

## Installation

1. Ensure you have Python 3.x installed on your system.
2. Clone the repository:

```bash
git clone https://gitlab.com/thegrayman/gvc.git
```

3. Install the required library:

```bash
pip install moviepy
```

## Usage
To use the gvc tool, run the following command:

```bash
python3 gvc.py <path_to_audio_file> <path_to_image_directory> <path_to_output_video_file>
```

## Example

```bash
python3 gvc.py blade_runner_blues.flac pics/ output.mp4
```
## Customization

To adjust the duration of the dissolve effect between images, modify the dissolve_duration variable in the gvc.py script. The default value is set to 1 second.

```python
dissolve_duration = 1  # Adjust this value to change the duration of the dissolve effect
```

## Dependencies

* Python 3.x
* moviepy library (pip install moviepy)

## Contributing

If you'd like to contribute to the project, please submit a merge request with your changes or open an issue to discuss the proposed changes.

## License

This project is licensed under the MIT License.

## Support

If you encounter any issues or require assistance, please open an issue on the GitLab project page.




