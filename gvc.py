#!/usr/bin/env python3

#****************************************************************************
#          Name: create_video.py
#       Version: 1.0
#        Author: Richard Gray <richard@rgray.info>
#   
#   Description: 
#   Just something to make my life easier.  It take the input of the name of
#   an audio file, and a directory of pictures, and the output file.  More   
#   details on the gitlab page. 
#****************************************************************************
import os
import sys
from moviepy.editor import *

def create_video(audio_file, image_dir, output_file):
    # Load audio file
    audio = AudioFileClip(audio_file)

    # Get image files from directory
    image_files = [os.path.join(image_dir, f) for f in os.listdir(image_dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
    image_files.sort()

    # Calculate duration for each image
    num_images = len(image_files)
    audio_duration = audio.duration
    image_duration = audio_duration / num_images

    # Set dissolve effect duration
    dissolve_duration = 3  # Adjust this value to change the duration of the dissolve effect

    # Load images, set their duration, and apply dissolve effect
    image_clips = [ImageClip(f).set_duration(image_duration).crossfadein(dissolve_duration) for f in image_files]

    # Concatenate image clips and set audio
    video = concatenate_videoclips(image_clips, method='compose')
    video = video.set_audio(audio)

    # Write video to output file
    video.write_videofile(output_file, codec='libx264', audio_codec='aac', fps=24)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: python3 gvc.py <audio_file> <image_directory> <output_video_file>")
        sys.exit(1)

    audio_file = sys.argv[1]
    image_dir = sys.argv[2]
    output_file = sys.argv[3]

    create_video(audio_file, image_dir, output_file)

